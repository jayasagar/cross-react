Web and Native Playground
===

This project is not a product, but a playground to test out various methodologies and principles
of application development in React and React Native, and to experiment with optimal amount of
shared code between Web, iOS and Android to achieve business objectives and user experience.
